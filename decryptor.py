import math
import re


# for some reason, vscode decided to insert 8 spaces instaed of 4 on tab...
# I figure this makes the code clearer, and I don't really want to fix it, so...

wat = 'goapsesnrbvltoameyeeefseaoctptsntuoeohitjhmdnig'
wat_2 = 'tlethgytecnfoeneenolaroictepamuonytloexosemcdphemttfsnuadtephstitstear'
double_wat = 'tlctubnseweieestheaorstucingaeiiitusihbttaenxosseretedohmndtnfldcwehirteetetioaoeiwryhvrhnx'
print(len(wat))

# basic rail decrypter
def rails_d(c, posts):
        ''' c = ciphertext, posts = f (across), rails = len/f (up/down) '''
        str = ''
        # get the number of rails, accounting for strings less than full fence (posts*rails)
        rails = math.ceil(len(c) / posts)
        
        # for each post of the fence
        for i in range(0,rails):
                # get all the letters going across the jth rail of the fence
                # every "rails"th + i char in c goes in the rail
                for j in range(0, len(c), rails):
                        if (j+i < len(c)):
                                str += c[j+i]
        return str

### PART ONE #1
for i in range(1,len(wat)):
        print(f'{i} : {rails_d(wat,i)}')

### PART ONE #2
# throw a space somewhere
def stick_padding_1(str, i):
        return str[:i] + ' ' + str[i:]

# brute force find place for a single space
for i in range(0, len(wat_2)):
        print(f'{i} : {rails_d(stick_padding_1(wat_2, i), 6)}')


# brute force 2 spaces....
def stick_padding_2(str, i):
        return str[:i] + '     ' + str[i:]

# does it by hand

print(len(wat_2))


# trying to brute force double padding...
for i in range(0, len(double_wat)):
        print(f'{i} : {rails_d(stick_padding_2(double_wat, i), 6)}')

# did it by hand...
triple_wat = 'tlctubnseweieestheaorstucingaei iitusihbttaenxo sseretedohmndtn fldcwehirteetet ioaoeiwryhvrhnx '
print(f'yoink : {rails_d(triple_wat, 6)}')


# dang...
# so for viginere, kjy apears to be 'the'
# kjyi might be then....
# huh
# xw1653-mvgqiiir-anx123
# anx123
# abc123...
#   cs1563
#   xw1653
# if the key is anything close to a word...
def getKeyLetter(a, b):
        a_num = ord(a)
        b_num = ord(b)
        return chr((b_num - a_num) % 26 + 97)

def guessKey(guess, c_part):
        out = ''
        for i in range(0, len(guess)):
                out += getKeyLetter(guess[i], c_part[i])
        return out

print(guessKey('crypto', 'giettt'))
# nope
print(guessKey('abcs', 'anxn'))
# ehh...

# xw1653-mvgqiiir-anx123, ayrrq vft123 vs kjyi cifo yjrrzvqv
# cs1653-????????-abc123, where abc123 is your pitt username
# ok...

# gets rid of any non letter characters
def strip(s):
        return re.sub(r'[^a-z]', '', s)

huh = strip('anx123, ayrrq vft123 vs kjyi cifo yjrrzvqv')
guess = strip('abc123, where abc123 is your pitt username')
print(huh)
print(guess)

print(guessKey(guess, huh))
# amvernamvernamvernamvernamver
# yeet...

all_of_it = '''
bsfq wamo za fuihzag mih urcdttkvns olzf mqnwrte. kjy ynvq xsdclqoiu ftqk xnb or
olzf aenmxamqix. kb cahtcrtq olv sizvp jgeb jj kue mnwztnyzrk, log hyjg sgwqzg
ta v fzgbgxovg rqksjvtamc kue ojhv (vn mic cnnspexr) fam xyr tajpj ghmo cfh uezh
kb dqxvpct fcmj zeenexr azy xyr pdzzzbue mezy fqigv zeenexr. cdzekr a bmmmntq
wmkouofik eebjwzgodt rrzep xw1653-mvgqiiir-anx123, ayrrq vft123 vs kjyi cifo
yjrrzvqv, go tjpu logm wlomunwzbn.

fj hvprkkx kue dvmc sezxi drsevkv, log kvfoangc lfep v fihtq asipe fcek geeoiu
iaddslf nghfves aa vrvle prkvl kjy wbuzy wfzefcmet rqvhrolq. nyszif olv popz cfh
uezh wbr fcmj gaef xf logm fzgbgxovg rqksjvtamc. zacxphv poyhiegs mn rvpeeneil
ta zbgyaui lfj yapv tbdq rsixs mn mk eexvxvf ta olv eaug jvacq xmgued.

os urcdttk ghun qvfsmbi, pbu bmssnbxt yjrd m fejvswd ionmuiekvoz, vru ghqi e
seufz jfecq jr ryl wzcj bf fci uvsojzveep giettt(n). wlomuo xyr cayi pbu gniu
sod wsku or olvfe eoigf ta tsle buoflpkqo vvcoedxfey. mbeza, izxplqe ojqdrnfn ej
aeozwjnrk os vkpxvme uoi tsle cayi nbrwn ej vt dzprgee os kue hdkvaedz gzchqm.

mw log yiteyboiu ghun qvfsmbi fe ttz vrvl rzrtr mqnwrte gnmet ezomirlk kie nnp
kegrr, kjy dny mirfgafz cfhr ijvb go qstcniz olv ftqkw pbu ovvivep jyk, nnp ngra
yapv ynnprvzgtqi hfpuyzrk nnp nyszif dx.

za euolve cmni, pbu ypwk funhmk n wddxvhp rdpv qeexvzoizb cfhr mktibaocij vn aii
kb tij treasmegus qvgy. vf kjy ynvq gmkglq ysthmqixrgiai esbuf tsle sagykvoz,
tsl zak rmju ta dqgyeyzrk n rmdp wrnoz fihtq asipe mih r oaedg bnsunoz
rxmhmentujr kb eznyir ttvx pbu szx whlx kszate.
'''

# vigenere decryptor
def vignere_d(c, k):
        # a count of alphabetical characters, helps keep track of which letter in the key to use
        count = 0
        out = ''
        # trvel the string
        for i in range(0, len(c)):
                # only decrypt alphabetical characters
                if (re.match(r'[a-z]', c[i])):
                        out += getKeyLetter(k[count % len(k)], c[i])
                        count += 1
                else:
                        out += c[i]
        return out

# not sure of the excat order of the key, so permute it just in case
def permuteKey(k):
        out = [k]
        for i in range(1, len(k)):
                kp = out[i-1]
                print(kp)
                kp = kp[1:] + kp[0]
                out.append(kp)
        return out

# who's this guy?
k_i = 'vernam'

# brute force the permutations
for k in permuteKey(k_i):
        print(f'{i} : {vignere_d(all_of_it, k)}')
# turns out the first one was right...


print('\n\n\n\nSolutions!!')
print('\nPart 1')
print(f'{6} : \'{rails_d(wat,6)}\'')
print(f'? : \'{rails_d(wat_2, 6)}\'')

print(f'{59} : \'{rails_d(stick_padding_1(wat_2, 59), 6)}\'')
triple_wat = 'tlctubnseweieestheaorstucingaei iitusihbttaenxo sseretedohmndtn fldcwehirteetet ioaoeiwryhvrhnx '
print(f'yoink : \'{rails_d(triple_wat, 6)}\'')
print('\nPart 2')
print(f'{i} : {vignere_d(all_of_it, k_i)}')
